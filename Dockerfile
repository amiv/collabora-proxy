from nginx

WORKDIR /app
COPY ./ ./

COPY collabora-proxy.conf /usr/local/share/collabora-proxy.conf
COPY docker-entrypoint.sh /usr/local/bin/

CMD [ "docker-entrypoint.sh" ]
